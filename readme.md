router-ob
==============

This repo contains ansible and packer configurations to build a simple three interface router, with limited routing ruleset.

It is intended for test deployments and exercises.

We are mostly following [this guide](https://www.openbsd.org/faq/pf/example1.html) for the openbsd docs.

Flags and parameters focus on
* NAT to uplink
* dhcp
* DNS (using unbound)
* ip addresses on subnets


To build
-----------

To build use `make ova`. This will build a new .ova file, using ansible for provisioning, using the values from `router-default.json`.

Alternative is to use `make BUILDVARS=other.json ova` whih will use the alternative variable file.

For the ansible part, generic stuff is put in `group_vars/all` and the host specific things are put in `host_vars` as per usual. Ansible will use the hostname specified in `HOST_NAME` in the vars file.

To create pages:
-------------------------

To create the associated documentation pages 

1. Upload files to gdrive using `make upload`

    This creates `download_links.yml` with share links

2. Build them using `make pages`. This will build the images, if applicable

3. Resulting html is in "site"

    You must manually push the repo to gitlab.

To add new configs
----------------------

There are currently three "default" builds

* `router-default`: NAT, DHCP and the default 111+112 subnets
* `router-nonat`: Just routing, DHCP and subnets 192.168.20.0/24 and 192.168.30.0/24
* `router-nodhcp`: NAT, no DHCP and the same subnets as `router-nonat`

To add a new 
----------------------

Adding a new profile follow this procedure

1. Duplicate one of the vars file, e.g. `cp ob70-router-default.json ob70-router-new.json`
2. Update new file 
2. Create the file `host_vars/ob70-router-new.yml` and add the parameters for the new router profile
3. Build new profile using `make ob70-router-new.ova`


To debug and such
--------------------

something about `ansible-playbook playbook.yml -i inventory -l router-default` and that "hostname" step fails.


To upload to gdrive
-----------

Usage: `make upload`

This will
1. Compare sha256 sums
2. Upload all ova files to gdrive, if no match

NB: There are some errors where you need to delete the files manually from gdrive. Also, sharing is not enable by default and we have redundant file on upload...

Setting up sharing is a manual process, as well as updating `downloads.yml`

Gdrive must be set up correctly, see [pydrive quick start](https://pythonhosted.org/PyDrive/quickstart.html)


Quirks
-----------------

To ensure the sequence of the ethernet adapters, the PCI slot number of each has been hardcoded. This might have side effects.

Currently, there are something with ansible/packer and the key algorithms. See issue [here](https://github.com/hashicorp/packer-plugin-ansible/issues/69).

Troubleshooting
-------------------------

To see what is happening while building, change the flag "headless" from true to false in [`build.json`](build.json)

If packer fails at the ansible step,

1. Log in to the VM
2. use `ifconfig` to get the host ip
3. create an inventory file with content like
```
ob70-router-default ansible_host=<host_ip>
```
4. run ansible manually by `ansible-playbook playbook.yml -i inventory -l ob70-router-default`

### Source path is invalid

The builds fails with a message resembling 

```
* source_path is invalid: stat ./base-images/ob70-base/ob70-base.vmx: no such file or directory
```

The path to the base image is wrong or missing.

I have a different [repo for base images](https://gitlab.com/moozer/packer-openbsd-base).

Import the .ova (import file format) file to create the needed .vmx (actual vm on disk). Ensure that the location is correct.

### Failed to connect to the host via scp

See [here](https://github.com/hashicorp/packer-plugin-ansible/issues/100) for related github issue. Seems to be a compatibility issue with newer versions of ssh.

Fixed by using the '-O' option to scp. See the [man page](https://man.openbsd.org/scp).


References
--------------

* [packer and vmware-vmx builder](https://www.packer.io/docs/builders/vmware-vmx.html)
* [packer and shell provisioning](https://www.packer.io/docs/provisioners/shell.html)
* [ansible docs on bsd](https://docs.ansible.com/ansible/latest/user_guide/intro_bsd.html)
