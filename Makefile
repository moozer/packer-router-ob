SHELL := /usr/bin/env bash

images_all = $(basename $(wildcard ob7?-router*.json))
ova_files_all = $(patsubst %, %.ova, $(images_all))
image_name = ob70-base # default build
tmp_dir = ./tmp
packer_cache_dir = packer_cache
build_template = build.json
build_vars = $(image_name).json
packer_log_file = packer.log
packer_debug_log_file = packer_debug.log

# system python interpreter. used only to create virtual environment
PY = python3
VENV = venv
BIN=$(VENV)/bin

.PHONY: help
help:
	@echo "Images to build: $(images_all)"
	@echo "Usage:"
	@echo "  make all: build everything"
	@echo "  make <ova-file>: build one"
	@echo "  make upload: upload to gdrive"
	@echo "  make pages: create website"
	@echo "  make clean: delete all artifacts"

.PHONY: image
image: $(image_name).ova

$(ova_files_all): %.ova: %.json $(build_template) playbook.yml host_vars/%.yml group_vars

%.ova:
	rm -rf output-$(basename $@)
	rm -f $(basename $@).ova
	rm -f $(packer_log_file)
	mkdir -p "$(packer_cache_dir)"
	mkdir -p "$(tmp_dir)"

	PACKER_LOG=1 \
	TMPDIR="$(tmp_dir)" \
	CHECKPOINT_DISABLE=1 \
	PACKER_CACHE_DIR="$(packer_cache_dir)" \
	packer build -on-error=abort -var-file=$(basename $@).json $(build_template) \
	> >(tee -a $(packer_log_file)) \
	2> >(tee -a $(packer_log_file) >&2)
	sha256sum $(basename $@).ova | awk '{print $$1}' > $(basename $@).ova.sha256

.PHONY: all
all: $(ova_files_all)
	echo "building all images:" $@

# set up virtual environment
$(VENV): requirements.txt
	$(PY) -m venv $(VENV)
	$(BIN)/pip install --upgrade -r requirements.txt
	touch $(VENV)

.PHONY: pages
pages: all $(VENV)
	echo localhost $(images_all) | tr ' ' '\n' > pages_inv
	ansible-playbook generate_pages.yml --extra-vars=@download_links.yml -i pages_inv --extra-vars="ansible_python_interpreter=$(shell which python3)"
	. venv/bin/activate; mkdocs build

.PHONY: upload
upload: $(ova_files_all)
	python3 upload.py $(images_all)

.PHONY: clean
clean:
	rm -rf output-*
	rm -rf *.ova
	rm -rf *.log
	rm -rf *.sha1
	rm -rf *.sha256
	rm -rf $(packer_cache_dir)
	rm -rf $(tmp_dir)
	rm -rf $(VENV)
	rm -rf mkdocs