from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
import yaml
import sys

download_yaml_file = "download_links.yml"

# fetches the content from gdrive and compares with local
def verify_sha( filename, id ):
  gdrive_file = drive.CreateFile({'id': id})
  gdrive_sha = gdrive_file.GetContentString().strip()
  print("Remote sha:", gdrive_sha)

  local_sha =  open(filename,'r').read().strip()
  print("local sha:", local_sha)

  return gdrive_sha == local_sha

# uploads new or replaces existing file
def upload_file( filename, current_files ):
  print( "Uploading", filename )

  if( filename in current_files.keys() ):
    print( "File already exists, reusing id")
    gdrive_file = drive.CreateFile({'id': current_files[filename]})
  else:
    gdrive_file = drive.CreateFile({'title': filename})

  gdrive_file.SetContentFile(filename)
  gdrive_file.Upload()

# sets sharing. No check of current state performed
def share_file( filename, current_files ):
  print( "Sharing", filename )

  gdrive_file = drive.CreateFile({'id': current_files[filename]})

  # Insert the permission.
  permission = gdrive_file.InsertPermission({
                          'type': 'anyone',
                          'value': 'anyone',
                          'role': 'reader'})

  print( f"Link for {filename} is", gdrive_file['alternateLink'])  # Display the sharable link.
  return gdrive_file['alternateLink']

# handles sha checking and uploads
def process_file( basename, current_files ):
  ova_filename = basename + '.ova'
  sha_filename = ova_filename +'.sha256'

  if( sha_filename in current_files.keys() ):
    print( f"Download {sha_filename}")
    sha_match = verify_sha( sha_filename, current_files[sha_filename] )
  else:
    print( f"{sha_filename} not found. Assuming upload is needed" )
    sha_match = False

  if( not sha_match ):
    print( "SHA256 mismatch uploading")
    upload_file( sha_filename, current_files )
    upload_file( ova_filename, current_files )
  else:
    print( "SHA256 match not uploading")

  link_sha = share_file( sha_filename, current_files )
  link_ova = share_file( ova_filename, current_files )

  return link_sha, link_ova

if __name__ == "__main__":
  if( len(sys.argv) < 2 ):
    print( f"usage: {sys.argv[0]} <file1> <file2> ...")
    exit(1)

  basenames = sys.argv[1:]

  print( "* Logging in" )
  gauth = GoogleAuth()
  gauth.LocalWebserverAuth() # Creates local webserver and auto handles authentication.
  drive = GoogleDrive(gauth)
  print( "logged in" )

  print( "* Get current gdrive list")
  file_list = drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
  current_files = {f["title"]: f["id"] for f in file_list}
  print( "Existing files", current_files )

  print( "* Uploading files and sha sums")
  share_list = {}
  for basename in basenames:
    print(f"** {basename}" )
    link_sha, link_ova = process_file( basename, current_files )
    share_list[basename+'.ova'] = link_ova
    share_list[basename+'.ova.sha256'] = link_sha

  print( "* All files processed")

  print( f"* Save links to yaml: {download_yaml_file}")

  print( share_list )
  with open( download_yaml_file, mode="wt", encoding="utf-8") as file:
    yaml.dump({'download_links': share_list }, file)
